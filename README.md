
# Norigin Media Candidate Challenge
This projects resolves the problem described at https://github.com/NoriginMedia/candidate-tester 

## Run the challenge

### `npm install`
### `npm start`

Runs the app in the development mode.
<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


## Assumptions
* The top and bottom bars doesn't have any behaviour.
* Is not possible to select a specific day in the Days Tab. The active day will be today, and it will display all the days of the current week.
* If a program name doesn't fit in the size of the box, it will be cut out.
* Yellow line indicating the current program updates periodically, however active programs doesn't update automatically (you might refresh the page to see new active programs).
* Tests are not necessary since they are out of the scope of this challenge.
* It is not relevant to provide a production build for this challenge, and the dev script provided should be enough.


## Author
Name: Guillermo Constantino
<br />
Email: guilleconstantino@gmail.com
