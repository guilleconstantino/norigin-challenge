import React, { Component } from "react";
import Menu from "./Menu";
import Schedule from "./Schedule";
import TabBar from "./TabBar";
import "./styles.css";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Menu />
        <Schedule />
        <TabBar />
      </div>
    );
  }
}

export default App;
