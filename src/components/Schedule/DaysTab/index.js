import React, { Component } from "react";
import { MdStar } from "react-icons/md";
import moment from "moment";
import "./styles.css";

class DaysTab extends Component {
  constructor(props) {
    super(props);
    const today = moment();

    this.state = {
      today: today,
      days: this.getDaysOfTheWeek()
    };
  }

  render() {
    const {days} = this.state;
    return (
      <div className="daysTab scheduleRow">
        <div className="scheduleRow__logoContainer text-center">
          <MdStar size={30} />
        </div>
        <div className="scheduleRow__daysHeader scroll">
          {days.map((day, i) => (
            <span key={i} className={`daysTab__day ${this.getSelectedClass(day)}`}>
              <span className="daysTab__dayName">{day.format("ddd")}</span>
              <span className="daysTab__dayInfo">{day.format("DD.MM.")}</span>
            </span>
          ))}
        </div>
      </div>
    );
  }

  getDaysOfTheWeek() {
    const startOfWeek = moment().startOf("isoWeek");
    const endOfWeek = moment().endOf("isoWeek");
    const days = [];
    let day = startOfWeek;

    while (day <= endOfWeek) {
      days.push(day);
      day = day.clone().add(1, "d");
    }

    return days;
  }

  getSelectedClass(day) {
    return this.state.today.isSame(day, 'day') ? "daysTab__day--selected" : ""
  }
}

export default DaysTab;
