import React, { Component } from "react";
import moment from "moment";

import Hour from "./Hour";
import { TIME_WIDTH } from "../../../helpers/consts";
import "./styles.css";

class HoursTab extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nowPosition: this.getNowPosition(),
      hours: Array.from(Array(24).keys())
    };
  }

  componentDidMount() {
    const ONE_MINUTE = 60000;
    this.interval = setInterval(this.updateTime.bind(this), ONE_MINUTE);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const { hours, nowPosition } = this.state;

    return (
      <div className="scheduleRow">
        <div className="scheduleRow__logoContainer" />
        <div className="scheduleRow__hoursheader">
          <div className="now" style={{ left: nowPosition }} />
          {hours.map((hour, i) => (
            <Hour hour={hour} key={i} />
          ))}
        </div>
        <button className="btn btn-warning position-fixed buttonNow" onClick={e => this.centerScroll()}>
          NOW
        </button>
      </div>
    );
  }

  getNowPosition() {
    const now = moment();
    const minutes = now.hours() * 60 + now.minutes();

    return minutes * TIME_WIDTH;
  }

  updateTime() {
    this.setState({ nowPosition: this.getNowPosition() });
  }

  centerScroll() {
    if (this.props.scrollEl) {
      const offset = window.innerWidth / 2 - 40;

      this.props.scrollEl.scrollTo(this.state.nowPosition - offset, 0);
    }
  }
}

export default HoursTab;
