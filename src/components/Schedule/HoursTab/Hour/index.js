import React, { Component } from "react";
import moment from "moment";
import { TIME_WIDTH } from "../../../../helpers/consts";
import "./styles.css";

class Hour extends Component {
  render() {
    const { hour } = this.props;
    const time = moment().startOf('day').add(hour, 'hour');
    
    return (
      <span className="hour" style={{width: (TIME_WIDTH*60) +'px'}}>
        <span className="hour__date">{time.format('HH:mm')}</span>
      </span>
    );
  }
}

export default Hour;
