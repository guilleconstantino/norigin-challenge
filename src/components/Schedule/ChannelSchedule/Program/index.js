import React, { Component } from "react";
import "./styles.css";
import moment from "moment";
import { TIME_WIDTH } from "../../../../helpers/consts";

class Program extends Component {
  render() {
    const { title, start, end } = this.props.schedule;
    const startTime = moment(start);
    const endTime = moment(end);
    const diff = endTime.diff(startTime, "minutes");

    const programClass = `program ${this.getActiveClass(startTime, endTime)}`;
    return (
      <span className={programClass} style={{ width: TIME_WIDTH * diff + "px" }}>
        <span className="program__title">{title}</span>
        <span className="program__date">
          {startTime.format("HH:mm")} - {endTime.format("HH:mm")}
        </span>
      </span>
    );
  }

  getActiveClass(startTime, endTime) {
    const active = moment().isBetween(startTime, endTime);
    return active ? "program--active" : "active";
  }
}

export default Program;
