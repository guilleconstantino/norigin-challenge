import React, { Component } from "react";
import "./styles.css";
import Program from "./Program";

class ChannelSchedule extends Component {
  render() {
    const { images, schedules } = this.props.channel;
    return (
      <div className="scheduleRow">
        <div className="scheduleRow__logoContainer scheduleRow__logoContainer--border">
          <img src={images.logo} className="scheduleRow__logo" />
        </div>
        <div className="scheduleRow__programs">
          {schedules.map((schedule, i) => (
            <Program key={i} schedule={schedule} />
          ))}
        </div>
      </div>
    );
  }
}

export default ChannelSchedule;
