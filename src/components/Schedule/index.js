import React, { Component } from "react";
import ChannelSchedule from "./ChannelSchedule";
import DaysTab from "./DaysTab";
import HoursTab from "./HoursTab";
import { SERVER_URL } from "../../helpers/consts";

import "./styles.css";

class Schedule extends Component {
  constructor(props) {
    super(props);

    this.state = {
      channels: [],
    };
  }

  componentDidMount() {
    const url = SERVER_URL;
    fetch(url)
      .then(result => result.json())
      .then(result => this.setState({ channels: result.channels }));    
  }

  render() {
    const { channels } = this.state;
    return (
      <div className="schedule">
        <DaysTab />
        <div className="scroll" ref={scrollEl => this.scrollEl = scrollEl}>
          <HoursTab scrollEl={this.scrollEl} />
          {channels.map((channel, i) => (
            <ChannelSchedule channel={channel} key={i} />
          ))}
        </div>
      </div>
    );
  }
}

export default Schedule;
