import React, { Component } from "react";
import "./styles.css";
import { MdHome, MdLiveTv, MdList, MdReplay, MdMap } from "react-icons/md";

class TabBar extends Component {
  render() {
    return (
      <div className="tabbar container-fluid">
        <div className="row align-items-start">
          <div className="col text-center">
            <MdHome size={30} />
          </div>
          <div className="col text-center">
            <MdLiveTv size={30} />
          </div>
          <div className="col text-center">
            <MdList size={30} className="selected" />
          </div>
          <div className="col text-center">
            <MdReplay size={30} />
          </div>
          <div className="col text-center">
            <MdMap size={30} />
          </div>
        </div>
      </div>
    );
  }
}

export default TabBar;
