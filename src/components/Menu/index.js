import React, { Component } from "react";
import { MdSearch, MdPerson } from "react-icons/md";
import logo from "./../../static/logo.png";
import "./styles.css";

class Menu extends Component {
  render() {
    return (
      <div className="menu container-fluid">
        <div className="row align-items-start">
          <div className="col">
            <MdPerson size={28} />
          </div>
          <div className="col text-center">
            <img alt="" src={logo} className="menu__logo" />
          </div>
          <div className="col text-right">
            <MdSearch size={28} />
          </div>
        </div>
      </div>
    );
  }
}

export default Menu;
