import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App.js";
import { hot } from "react-hot-loader";

const MyApp = hot(module)(App);

ReactDOM.render(<MyApp />, document.getElementById("root"));
